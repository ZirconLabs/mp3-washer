﻿namespace MP3_Washer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainLayout = new System.Windows.Forms.TableLayoutPanel();
            this.cleanButton = new System.Windows.Forms.Button();
            this.locationGroup = new System.Windows.Forms.GroupBox();
            this.locationLayout = new System.Windows.Forms.TableLayoutPanel();
            this.fileBrowseButton = new System.Windows.Forms.Button();
            this.locationText = new System.Windows.Forms.TextBox();
            this.folderBrowseButton = new System.Windows.Forms.Button();
            this.optionsGroup = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.backupCheck = new System.Windows.Forms.CheckBox();
            this.removeUniqueIDCheck = new System.Windows.Forms.CheckBox();
            this.removeCommentsCheck = new System.Windows.Forms.CheckBox();
            this.removePrivateCheck = new System.Windows.Forms.CheckBox();
            this.processList = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.logoPicture = new System.Windows.Forms.PictureBox();
            this.titleLabel = new System.Windows.Forms.Label();
            this.subtitleLabel = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.fileBrowser = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.mainLayout.SuspendLayout();
            this.locationGroup.SuspendLayout();
            this.locationLayout.SuspendLayout();
            this.optionsGroup.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // mainLayout
            // 
            this.mainLayout.ColumnCount = 2;
            this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.mainLayout.Controls.Add(this.cleanButton, 1, 4);
            this.mainLayout.Controls.Add(this.locationGroup, 0, 1);
            this.mainLayout.Controls.Add(this.optionsGroup, 0, 2);
            this.mainLayout.Controls.Add(this.processList, 0, 3);
            this.mainLayout.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.mainLayout.Controls.Add(this.progressBar, 0, 4);
            this.mainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainLayout.Location = new System.Drawing.Point(0, 0);
            this.mainLayout.Name = "mainLayout";
            this.mainLayout.RowCount = 5;
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.89219F));
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 59.10781F));
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.mainLayout.Size = new System.Drawing.Size(527, 476);
            this.mainLayout.TabIndex = 1;
            // 
            // cleanButton
            // 
            this.cleanButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cleanButton.Enabled = false;
            this.cleanButton.Location = new System.Drawing.Point(449, 437);
            this.cleanButton.Name = "cleanButton";
            this.cleanButton.Size = new System.Drawing.Size(75, 36);
            this.cleanButton.TabIndex = 0;
            this.cleanButton.Text = "Clean";
            this.cleanButton.UseVisualStyleBackColor = true;
            this.cleanButton.Click += new System.EventHandler(this.cleanButton_Click);
            // 
            // locationGroup
            // 
            this.mainLayout.SetColumnSpan(this.locationGroup, 2);
            this.locationGroup.Controls.Add(this.locationLayout);
            this.locationGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.locationGroup.Location = new System.Drawing.Point(3, 79);
            this.locationGroup.Name = "locationGroup";
            this.locationGroup.Size = new System.Drawing.Size(521, 50);
            this.locationGroup.TabIndex = 2;
            this.locationGroup.TabStop = false;
            this.locationGroup.Text = "Select Files";
            // 
            // locationLayout
            // 
            this.locationLayout.ColumnCount = 3;
            this.locationLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.locationLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.locationLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.locationLayout.Controls.Add(this.fileBrowseButton, 1, 0);
            this.locationLayout.Controls.Add(this.locationText, 0, 0);
            this.locationLayout.Controls.Add(this.folderBrowseButton, 2, 0);
            this.locationLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.locationLayout.Location = new System.Drawing.Point(3, 16);
            this.locationLayout.Name = "locationLayout";
            this.locationLayout.RowCount = 1;
            this.locationLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.locationLayout.Size = new System.Drawing.Size(515, 31);
            this.locationLayout.TabIndex = 0;
            // 
            // fileBrowseButton
            // 
            this.fileBrowseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fileBrowseButton.Location = new System.Drawing.Point(356, 3);
            this.fileBrowseButton.Name = "fileBrowseButton";
            this.fileBrowseButton.Size = new System.Drawing.Size(75, 25);
            this.fileBrowseButton.TabIndex = 0;
            this.fileBrowseButton.Text = "Files";
            this.fileBrowseButton.UseVisualStyleBackColor = true;
            this.fileBrowseButton.Click += new System.EventHandler(this.fileBrowseButton_Click);
            // 
            // locationText
            // 
            this.locationText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.locationText.Location = new System.Drawing.Point(3, 3);
            this.locationText.Name = "locationText";
            this.locationText.ReadOnly = true;
            this.locationText.Size = new System.Drawing.Size(347, 20);
            this.locationText.TabIndex = 1;
            // 
            // folderBrowseButton
            // 
            this.folderBrowseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.folderBrowseButton.Location = new System.Drawing.Point(437, 3);
            this.folderBrowseButton.Name = "folderBrowseButton";
            this.folderBrowseButton.Size = new System.Drawing.Size(75, 25);
            this.folderBrowseButton.TabIndex = 2;
            this.folderBrowseButton.Text = "Folder";
            this.folderBrowseButton.UseVisualStyleBackColor = true;
            this.folderBrowseButton.Click += new System.EventHandler(this.folderBrowseButton_Click);
            // 
            // optionsGroup
            // 
            this.mainLayout.SetColumnSpan(this.optionsGroup, 2);
            this.optionsGroup.Controls.Add(this.tableLayoutPanel1);
            this.optionsGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optionsGroup.Location = new System.Drawing.Point(3, 135);
            this.optionsGroup.Name = "optionsGroup";
            this.optionsGroup.Size = new System.Drawing.Size(521, 117);
            this.optionsGroup.TabIndex = 3;
            this.optionsGroup.TabStop = false;
            this.optionsGroup.Text = "Options";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.backupCheck, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.removeUniqueIDCheck, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.removeCommentsCheck, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.removePrivateCheck, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(515, 98);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // backupCheck
            // 
            this.backupCheck.AutoSize = true;
            this.backupCheck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.backupCheck.Location = new System.Drawing.Point(3, 3);
            this.backupCheck.Name = "backupCheck";
            this.backupCheck.Size = new System.Drawing.Size(509, 17);
            this.backupCheck.TabIndex = 0;
            this.backupCheck.Text = "Make Backups Before Cleaning (.bak)";
            this.backupCheck.UseVisualStyleBackColor = true;
            // 
            // removeUniqueIDCheck
            // 
            this.removeUniqueIDCheck.AutoSize = true;
            this.removeUniqueIDCheck.Checked = true;
            this.removeUniqueIDCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.removeUniqueIDCheck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.removeUniqueIDCheck.Location = new System.Drawing.Point(3, 49);
            this.removeUniqueIDCheck.Name = "removeUniqueIDCheck";
            this.removeUniqueIDCheck.Size = new System.Drawing.Size(509, 17);
            this.removeUniqueIDCheck.TabIndex = 4;
            this.removeUniqueIDCheck.Text = "Remove Unique File ID";
            this.removeUniqueIDCheck.UseVisualStyleBackColor = true;
            // 
            // removeCommentsCheck
            // 
            this.removeCommentsCheck.AutoSize = true;
            this.removeCommentsCheck.Checked = true;
            this.removeCommentsCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.removeCommentsCheck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.removeCommentsCheck.Location = new System.Drawing.Point(3, 72);
            this.removeCommentsCheck.Name = "removeCommentsCheck";
            this.removeCommentsCheck.Size = new System.Drawing.Size(509, 23);
            this.removeCommentsCheck.TabIndex = 3;
            this.removeCommentsCheck.Text = "Remove Comments (May have identifiable information)";
            this.removeCommentsCheck.UseVisualStyleBackColor = true;
            // 
            // removePrivateCheck
            // 
            this.removePrivateCheck.AutoSize = true;
            this.removePrivateCheck.Checked = true;
            this.removePrivateCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.removePrivateCheck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.removePrivateCheck.Location = new System.Drawing.Point(3, 26);
            this.removePrivateCheck.Name = "removePrivateCheck";
            this.removePrivateCheck.Size = new System.Drawing.Size(509, 17);
            this.removePrivateCheck.TabIndex = 5;
            this.removePrivateCheck.Text = "Remove Private Tags";
            this.removePrivateCheck.UseVisualStyleBackColor = true;
            // 
            // processList
            // 
            this.mainLayout.SetColumnSpan(this.processList, 2);
            this.processList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.processList.FormattingEnabled = true;
            this.processList.Location = new System.Drawing.Point(3, 258);
            this.processList.Name = "processList";
            this.processList.Size = new System.Drawing.Size(521, 173);
            this.processList.TabIndex = 4;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.mainLayout.SetColumnSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.logoPicture, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.titleLabel, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.subtitleLabel, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(521, 70);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // logoPicture
            // 
            this.logoPicture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logoPicture.Image = ((System.Drawing.Image)(resources.GetObject("logoPicture.Image")));
            this.logoPicture.Location = new System.Drawing.Point(3, 3);
            this.logoPicture.Name = "logoPicture";
            this.tableLayoutPanel2.SetRowSpan(this.logoPicture, 2);
            this.logoPicture.Size = new System.Drawing.Size(64, 64);
            this.logoPicture.TabIndex = 0;
            this.logoPicture.TabStop = false;
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.titleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(73, 0);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(445, 35);
            this.titleLabel.TabIndex = 1;
            this.titleLabel.Text = "MP3 Washer";
            this.titleLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // subtitleLabel
            // 
            this.subtitleLabel.AutoSize = true;
            this.subtitleLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.subtitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subtitleLabel.Location = new System.Drawing.Point(73, 35);
            this.subtitleLabel.Name = "subtitleLabel";
            this.subtitleLabel.Size = new System.Drawing.Size(445, 35);
            this.subtitleLabel.TabIndex = 2;
            this.subtitleLabel.Text = "Strips identifiers and other tracking tags from MP3 files.\r\nCopyright © 2013 by J" +
    "ames Yoder";
            this.subtitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // progressBar
            // 
            this.progressBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar.Location = new System.Drawing.Point(3, 437);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(440, 36);
            this.progressBar.TabIndex = 6;
            this.progressBar.Visible = false;
            // 
            // fileBrowser
            // 
            this.fileBrowser.DefaultExt = "mp3";
            this.fileBrowser.Filter = "Mp3 Files|*.mp3";
            this.fileBrowser.Multiselect = true;
            this.fileBrowser.SupportMultiDottedExtensions = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 476);
            this.Controls.Add(this.mainLayout);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "MP3 Washer";
            this.mainLayout.ResumeLayout(false);
            this.locationGroup.ResumeLayout(false);
            this.locationLayout.ResumeLayout(false);
            this.locationLayout.PerformLayout();
            this.optionsGroup.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoPicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainLayout;
        private System.Windows.Forms.Button cleanButton;
        private System.Windows.Forms.GroupBox locationGroup;
        private System.Windows.Forms.TableLayoutPanel locationLayout;
        private System.Windows.Forms.Button fileBrowseButton;
        private System.Windows.Forms.TextBox locationText;
        private System.Windows.Forms.Button folderBrowseButton;
        private System.Windows.Forms.GroupBox optionsGroup;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.CheckBox backupCheck;
        private System.Windows.Forms.CheckBox removeUniqueIDCheck;
        private System.Windows.Forms.CheckBox removeCommentsCheck;
        private System.Windows.Forms.CheckBox removePrivateCheck;
        private System.Windows.Forms.ListBox processList;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox logoPicture;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label subtitleLabel;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.OpenFileDialog fileBrowser;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
    }
}

