﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace MP3_Washer
{
    public partial class MainForm : Form
    {
        private string[] files = null;
        private string folder = null;
        
        public MainForm()
        {
            InitializeComponent();
        }

        private void fileBrowseButton_Click(object sender, EventArgs e)
        {
            fileBrowser.FileName = string.Empty;
            if (fileBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                folder = null;
                files = fileBrowser.FileNames;
                locationText.Text = string.Join(", ", fileBrowser.FileNames);
                cleanButton.Enabled = true;
            }
        }

        private void folderBrowseButton_Click(object sender, EventArgs e)
        {
            folderBrowser.SelectedPath = string.Empty;
            if (folderBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                files = null;
                folder = folderBrowser.SelectedPath;
                locationText.Text = folderBrowser.SelectedPath;
                cleanButton.Enabled = true;
            }
        }

        private void cleanButton_Click(object sender, EventArgs e)
        {
            processList.Items.Clear();
            progressBar.Value = 0;
            progressBar.Visible = true;

            if (files == null && folder != null)
            {
                files = Directory.GetFiles(folder, "*.mp3", SearchOption.AllDirectories);
            }

            progressBar.Maximum = files.Length;
            foreach (var file in files)
            {
                if (backupCheck.Checked && !File.Exists(file + ".bak"))
                {
                    File.Copy(file, file + ".bak");
                }

                Clean(file);
                processList.Items.Add(file);
                progressBar.PerformStep();
                Application.DoEvents();
            }
            progressBar.Value = progressBar.Maximum;
        }

        public void Clean(string file)
        {
            var mp3 = new Mp3Lib.Mp3File(file);
            var removeTags = new List<Id3Lib.Frames.FrameBase>();

            // Get any tags we want to remove
            if (removePrivateCheck.Checked)
            {
                removeTags.AddRange(mp3.TagModel.Where(tag => tag.FrameId == "PRIV"));
            }
            if (removeUniqueIDCheck.Checked)
            {
                removeTags.AddRange(mp3.TagModel.Where(tag => tag.FrameId == "UFID"));
            }
            if (removeCommentsCheck.Checked)
            {
                removeTags.AddRange(mp3.TagModel.Where(tag => tag.FrameId == "COMM"));
            }

            // Now get rid of them
            foreach (var tag in removeTags)
            {
                mp3.TagModel.Remove(tag);
            }

            // Save the file without the removed tags
            mp3.Update();
        }
    }
}
